<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('paciente', 'PacienteController', ['only' => ['index']]);
Route::resource('seguro.paciente', 'PacienteSeguroController',['only'=>['update','store','show','destroy','index']]);
Route::resource('seguro', 'SeguroController',['only'=>['update','store','show','destroy','index']]);
Route::resource('consulta.paciente', 'PacienteConsultaController',['only'=>['update','store','show','destroy','index']]);
Route::resource('seguro', 'SeguroController',['only'=>['update','store','show','destroy','index']]);
Route::resource('consulta', 'ConsultaController',['only'=>['update','store','show','destroy','index']]);

