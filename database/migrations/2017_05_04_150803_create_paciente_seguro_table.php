<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteSeguroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes_seguros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seguro_id')->unsigned();
            $table->integer('paciente_id')->unsigned();
            $table->string('condicion');
            $table->foreign('seguro_id')->references('id')->on('seguros');
            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes_seguros');
    }
}
