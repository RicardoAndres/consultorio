<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    //

    protected $table = 'consultas';

    protected $fillable = ['cedula','seguro_id', 'fecha'];

    protected $hiddem = ['created_ad', 'updated_ad'];


     public function seguro()
    {
    	return $this->belongsTo('App\Seguro');
    }

    public function paciente()
    {
    	return $this->belongsTo('App\Paciente');
    }
    
}
