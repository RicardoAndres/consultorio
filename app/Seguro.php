<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seguro extends Model
{
    //

    protected $table = 'seguros';

    protected $fillable = ['nombre'];

    protected $hiddem = ['created_ad', 'updated_ad'];


    public function pacientes_seguros()
    {
    	return $this->hasMany('App\Paciente_Seguro');
    }

    public function consultas()
    {
    	return $this->hasMany('App\Consulta');
    }
}
