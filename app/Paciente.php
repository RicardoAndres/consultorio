<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    //

    protected $table = 'pacientes';

    protected $fillable = ['cedula','nombre', 'apellido'];

    protected $hiddem = ['created_ad', 'updated_ad'];


 	public function pacientes_seguros()
    {
    	return $this->hasMany('App\Paciente_Seguro');
    }

    public function consultas()
    {
    	return $this->hasMany('App\Consulta');
    }
}
