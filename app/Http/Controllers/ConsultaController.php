<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Paciente;
use App\Seguro;
use App\Paciente_Seguro;
use App\Consulta;

class ConsultaController extends Controller
{
    //
	public function store(Request $request)
	{
    	$seguro = Seguro::find($request->seguro_id);
    	$paciente = Paciente::find($request->paciente_id);

    	if ((!$seguro)||(!$paciente)) {

	   		return response()->json(['Mensaje' => 'Seguro o Paciente no Encontrados!.' , 'Codigo' => 404], 404);
    	}
    	else {

			$busq = DB::table('pacientes_seguros')->where('seguro_id', $request->seguro_id)->where('paciente_id',$request->paciente_id)->first();

    		if (!$busq) {
	   			return response()->json(['Mensaje' => 'El paciente no se encuentra registrado en ese seguro!.' , 'Codigo' => 404], 404);
    		} else {
	    			
	    			$busq = DB::table('consultas')->where('seguro_id', $request->seguro_id)->where('paciente_id',$request->paciente_id)->where('fecha', $request->fecha)->first();

				if ($busq) {
		   			return response()->json(['Mensaje' => 'El usuario ya posee una cita registrada!.' , 'Codigo' => 404], 404);	
				} 
				else {

					$consulta = new Consulta();
					$consulta->seguro_id = $request->seguro_id;
					$consulta->paciente_id = $request->paciente_id;
					$consulta->fecha = $request->fecha;
					$consulta->save();
		   			return response()->json([ 'Datos' => ['Seguro' => $seguro->nombre, 'Cedula' => $paciente->cedula, 'Fecha' => $consulta->fecha ], 'Mensaje' => 'Consulta Registrada con Exito!.', 'Codigo' => 200], 200);	
				}	
			}	
		}
	}






}
