<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Paciente;
use App\Seguro;
use App\Paciente_Seguro;

class PacienteSeguroController extends Controller
{
    //

    public function index($id_s)
    {
    	$seguro = Seguro::find($id_s);

    	if ($seguro) {

    		$busq = DB::table('pacientes_seguros')->where('seguro_id', $seguro->id)->pluck('paciente_id');

    		if ($busq->count() != 0) {
    			$busq = $busq->toArray();
    			$pacientes = DB::table('pacientes')->whereIn('id', $busq)->get();
	   			return response()->json(['Datos' => ['Seguro: ' => $seguro->nombre , 'Pacientes: ' => $pacientes ], 'Codigo' => 200], 200);
    			
    		} else {
	   			return response()->json(['Mensaje' => 'El seguro: '.$seguro->nombre.' no tiene pacientes en el registro.', 'Codigo' => 404], 404);
    		}
    		
    	} else {
	   		return response()->json(['Mensaje' => 'El seguro no se encuentra!', 'Codigo' => 404], 404);
    	}	
    }

    public function store(Request $request, $id_s)
    {
    	$seguro = Seguro::find($id_s);

    	if ($seguro) {

    		$search = DB::table('pacientes')->where('cedula', $request->cedula)->first();

    		if (!$search) {
    			$paciente = new Paciente();
    			$paciente->cedula = $request->cedula;
    			$paciente->nombre = $request->nombre;
    			$paciente->apellido = $request->apellido;
    			$paciente->save();

    			$pac_seg = new Paciente_Seguro();
    			$pac_seg->seguro_id = $id_s;
    			$pac_seg->paciente_id = $paciente->id;
    			$pac_seg->condicion = $request->condicion;

    			#dd($pac_seg);
    			$pac_seg->save();
	   			return response()->json(['Mensaje' => 'Se registro al paciente: '.$paciente->nombre.' en el seguro '.$seguro->nombre, 'Codigo' => 200], 200);	

    		} 
    		else {    			
    			$busq = DB::table('pacientes_seguros')->where('seguro_id', $id_s)->where('paciente_id', $search->id);	

    			if ($busq->count() == 0) {
    				$pac_seg = new Paciente_Seguro();
	    			$pac_seg->seguro_id = $id_s;
	    			$pac_seg->paciente_id = $search->id;
	    			$pac_seg->condicion = $request->condicion;
	    			$pac_seg->save();

	   				return response()->json(['Mensaje' => 'Se registro al paciente: '.$search->nombre.' en el seguro '.$seguro->nombre, 'Codigo' => 200], 200);	

    			} else {
	   				return response()->json(['Mensaje' => 'El paciente ya se encuentra registrado en el seguro: '.$seguro->nombre, 'Codigo' => 404], 404);	
    			}
    			
    		}


    	} else {
	   		return response()->json(['Mensaje' => 'El seguro no se encuentra!', 'Codigo' => 404], 404);
    	}
    	

    }


    public function show($id_s, $id_p)
    {
    	$seguro = Seguro::find($id_s);
    	$paciente = Paciente::find($id_p);

    	if ((!$seguro)||(!$paciente)) {
	   		return response()->json(['Mensaje' => 'El seguro o el paciente esta errado o no se encuentra registrado!', 'Codigo' => 404], 404);
    	} else {
    		
    		$busq = DB::table('pacientes_seguros')->where('seguro_id', $id_s)->where('paciente_id', $id_p);

    		if ($busq->count() == 0) {
	   			return response()->json(['Mensaje' => 'El paciente no se encuentra registrado en el seguro', 'Codigo' => 404], 404);
    			
    		} else {
	   			return response()->json(['Datos' => ['Seguro: ' => $seguro->nombre, 'Paciente: ' => $paciente], 'Codigo' => 200], 200);
    		}

    	}
    }

    public function update(Request $request, $id_s, $id_p)
    {
    	$seguro = Seguro::find($id_s);
    	$paciente = Paciente::find($id_p);

    	if ((!$seguro)||(!$paciente)) {
	   		return response()->json(['Mensaje' => 'El seguro o el paciente esta errado o no se encuentra registrado!', 'Codigo' => 404], 404);
    	} else {
    		
    		$busq = DB::table('pacientes_seguros')->where('seguro_id', $id_s)->where('paciente_id', $id_p)->first();
    		

    		if (!$busq) {
	   			return response()->json(['Mensaje' => 'El paciente no se encuentra registrado en el seguro', 'Codigo' => 404], 404);
    			
    		} else {

    			DB::table('pacientes_seguros')->where('id', $busq->id)->update(['condicion' => $request->condicion]);
    			
    			$paciente->nombre = $request->nombre;
    			$paciente->apellido = $request->apellido;
    			$paciente->cedula = $request->cedula;
    			$paciente->save();
	   			return response()->json(['Mensaje' => 'Se actualizaron los datos de '.$paciente->nombre .' en el seguro '.$seguro->nombre, 'Codigo' => 200], 200);
    		}

    	}

    }

    public function destroy($id_s, $id_p)
    {
    	$seguro = Seguro::find($id_s);
    	$paciente = Paciente::find($id_p);

    	if ((!$seguro)||(!$paciente)) {
	   		return response()->json(['Mensaje' => 'El seguro o el paciente esta errado o no se encuentra registrado!', 'Codigo' => 404], 404);
    	} else {
    		
    		$busq = DB::table('pacientes_seguros')->where('seguro_id', $id_s)->where('paciente_id', $id_p)->first();
    		

    		if (!$busq) {
	   			return response()->json(['Mensaje' => 'El paciente no se encuentra registrado en el seguro', 'Codigo' => 404], 404);
    			
    		} else {
    			$pac_seg = Paciente_Seguro::find($busq->id);
    			$pac_seg->delete();
	   			return response()->json(['Mensaje' => 'Se ha eliminado el Registro en '.$seguro->nombre, 'Codigo' => 200], 200);
    		}

    	}

    }

}
