<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Paciente;
use App\Seguro;
use App\Paciente_Seguro;

class SeguroController extends Controller
{
    //
    public function index()
    {
    	$seguros = Seguro::all();
	   	return response()->json(['Datos' => $seguros->all(), 'Codigo' => 200], 200);

    }


    public function store(Request $request)
    {
    	$nom_seguro = $request->nombre;

    	$busq = DB::table('seguros')->where('nombre',$nom_seguro)->first();

    	if ($busq) {
	   		return response()->json(['Mensaje' => 'El Seguro: '.$nom_seguro.' ya existe!.' , 'Codigo' => 404], 404);	
    	}
    	else {

    		$seguro = new Seguro($request->all());
    		$seguro->save();
	   		return response()->json(['Mensaje' => 'El Seguro: '.$nom_seguro.' se ha registrado.' , 'Codigo' => 200], 200);	
    	}

    }

    public function show($id_s)
    {
    	$seguro = Seguro::find($id_s);

    	if ($seguro) {
	   		return response()->json(['Datos' => $seguro->all(), 'Codigo' => 200], 200);
    	}
    	else{
	   		return response()->json(['Mensaje' => 'El Seguro no se encuentra!.' , 'Codigo' => 404], 404);	
    	}
    }

    public function destroy($id_s)
    {
    	$seguro = Seguro::find($id_s);

    	if ($seguro) {
    		$seguro->delete();
	   		return response()->json(['Mensaje' => 'El Seguro fue eliminado de la base de datos' , 'Codigo' => 200], 200);	
    	}
    	else {
	   		return response()->json(['Mensaje' => 'Seguro no encontrado!' , 'Codigo' => 404], 404);	
    	}
    }

    public function update(Request $request , $id_s)
    {
    	$seguro = Seguro::find($id_s);

    	if ($seguro) {

    			$seguro->fill($request->all());
    			$seguro->save();

	   		return response()->json(['Mensaje' => 'El Seguro: '.$request->nombre.' fue actualizado de la base de datos' , 'Codigo' => 200], 200);	
    	}
    	else {
	   		return response()->json(['Mensaje' => 'Seguro no encontrado!' , 'Codigo' => 404], 404);	
    	}
    }

    

}
