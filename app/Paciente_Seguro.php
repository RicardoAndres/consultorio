<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente_Seguro extends Model
{
    //

    protected $table = 'pacientes_seguros';

    protected $fillable = ['seguro_id', 'cedula_id', 'condicion'];

    protected $hiddem = ['created_ad', 'updated_ad'];


    public function seguro()
    {
    	return $this->belongsTo('App\Seguro');
    }

    public function paciente()
    {
    	return $this->belongsTo('App\Paciente');
    }
    
}
